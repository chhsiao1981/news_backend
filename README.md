記者快抄 backend
==============

記者快抄後端

### 執行內容：
1. 每10分鐘執行2支process: 1. crawler 2. summarizer

### 檔案存放位置：
&nbsp;&nbsp;爬下來的文章：

&nbsp;&nbsp;&nbsp;&nbsp;．staging: /volume/justcopy-staging/crawled

&nbsp;&nbsp;&nbsp;&nbsp;．prod: /volume/justcopy/crawled

&nbsp;&nbsp;產生的post markdown檔：

&nbsp;&nbsp;&nbsp;&nbsp;．staging: /volume/justcopy-staging/posts

&nbsp;&nbsp;&nbsp;&nbsp;．prod: /volume/justcopy/posts

&nbsp;&nbsp;static website位置：

&nbsp;&nbsp;&nbsp;&nbsp;．prod: /volume/justcopy/site

&nbsp;&nbsp;新版static website位置：

&nbsp;&nbsp;&nbsp;&nbsp;．staging: /volume/justcopy-staging/newsite/prod/latest

&nbsp;&nbsp;&nbsp;&nbsp;．prod: /volume/justcopy/newsite/prod/latest


### 開發方法：
0. login into ws1/2/3
1. git clone git@gitlab.corp.ailabs.tw:coldsheep/news_backend.git ~/
2. cd news_backend/containers
3. docker build -f Dockerfile_local -t news_backend_dev . (takes ~15 mins)
4. (進docker裡開發) (your_user_name記得修改) docker run -v /volume/justcopy-staging/news_data:/root/data -v /volume/justcopy-staging/crawled:/crawled -v /home/your_user_name/news_backend_db/containers:/justcopy -v /volume/justcopy-staging/posts:/root/_posts -it news_backend_dev /bin/bash
5. cp /root/data/db/mysql_connector_staging.py utils/db/mysql_connector.py
6. (執行) **root@06531e8b82dc:/justcopy#** python3 journalist.py 
7. add you code under /justcopy

### db schema:
[schema](https://gitlab.corp.ailabs.tw/coldsheep/news_backend/snippets/4)

### 執行方法：
##### 爬文: 
`python3 journalist.py`
##### 產生摘要: 
`python3 journalist.py gen-summary`

##### 測試方法：

&nbsp;&nbsp;&nbsp;&nbsp;crawl single article: (會寫入到db)

&nbsp;&nbsp;&nbsp;&nbsp;`python3 journalist.py M.1511607114.A.3AF Gossiping` (M.1511607114.A.3AF為文章id)
    
&nbsp;&nbsp;&nbsp;&nbsp;generate summary of single article:  (會寫入到db)

&nbsp;&nbsp;&nbsp;&nbsp;`python3 journalist.py gen-single M.1511607114.A.3AF Gossiping` 
  
  
&nbsp;&nbsp;&nbsp;&nbsp;dry run: (不會寫入到db)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;crawl single article:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`python3 journalist.py crawl-single M.1511607114.A.3AF Gossiping dryrun`
      
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;crawl all:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`python3 journalist.py crawl dryrun`
      
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;generate summary of single article:  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`python3 journalist.py gen-single M.1511607114.A.3AF Gossiping dryrun`
      
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;generate summary of all articles:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`python3 journalist.py gen-summary dryrun`
      