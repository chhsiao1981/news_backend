# -*- coding: utf-8 -*- 
from utils.crawler import PttWebCrawler
from utils.db.mysql_connector import DBManager
from utils.news_generator import News_Generator
from utils.ptt_filter import ArticleFilter
from utils.model_interface import Interface
from DrQA.scripts.retriever.engine import SearchEngine
from utils.analyzer import Analyzer
import requests
import urllib
import datetime
import os
import sys
import jieba
import time
import subprocess
import re
import os
import json

dict_path = os.path.join(os.getenv("JIEBA_DATA"), "dict.txt.big") 
jieba.set_dictionary(dict_path)
dbManager = DBManager()
crawler = PttWebCrawler(dbManager)
analyzer = Analyzer()
Filter = ArticleFilter()
# interface = Interface()
engine = SearchEngine(os.getenv('DOC'), os.getenv('TFIDF_DATA'))
REGULAR_THRESHOLD = 30
HOT_THRESHOLD = 40

def get_hot_boards():
    with open ('lists/hot_board.list') as f:
        content = f.readlines()
        hot_board = [ x.strip() for x in content ] 
        return hot_board

def get_board_lists():
    cat_boards = get_boards()
    boards = []
    for cat in cat_boards:
        crawl_board = [ x.strip() for x in cat_boards[cat] ] 
        boards.extend(crawl_board)
    return boards

def get_boards():
    with open ('lists/board.json', 'r', encoding='utf-8') as f:
        lines = f.readlines()
        content = ''.join(lines)
        boards = json.loads(content)
        return boards

def get_category(board):
    boards = get_boards()
    for cat in boards:
        crawl_board = [ x.strip() for x in boards[cat] ]
        if board in crawl_board:
            return cat 
    
def find_images(title, image_urls, urls, use_image_database, response):
    def get_url(texts):
        for text in texts:
            img_urls = analyzer.get_url(text)
            for url in img_urls:
                image_url = analyzer.open_url(url)
                if image_url != None:
                    return image_url

    def search_database():
        query = ' '.join(jieba.cut(title, cut_all=False)).strip()
        #print('search:',query)
        try:
            search_titles, search_texts = engine.process(query, k=20)
            #print('result:', search_titles)
            url = get_url(search_texts)
            if url == None:
                print('Database not found any url!')
            else:
                image_urls.append(url)
        except:
            pass

    for url in urls['article']:
        image_url = analyzer.open_url(url)
        if image_url != None:
            image_urls.append(image_url)

    if response:
        for url in urls['response']:
            image_url = analyzer.open_url(url)
            if image_url != None:
                image_urls.append(image_url)

    if use_image_database and len(image_urls) == 0:     
        search_database() 


def generate_post(board, article, summary, keywords, response, title, paragraph, article_url, url, image_url, category):
    '''
    Generate the post for front-end website
    Args:
        board: string
        article: dict, the original article
        sumamry, response: not used, may be useful
        title, paragraph: string, the title and body of the news
        article_url: string, the link to the original article
        url: dict, the urls in article and responses.
        image_url: list, the image urls
    '''
    YOUTUBE_DOMAIN = ['https://www.youtube.com', 'https://youtu.be']
    def add_post_head():
        original_title = article['Title'].replace('\\', '\\\\').replace('"', '\\"')
        collections = get_collections()
        keywords.extend(collections)
        tags = ' '.join(keywords)
        res.append('---\n')
        res.append('layout: post\n')
        res.append("tags: %s\n" % tags)
        res.append("categories: %s %s\n" % (category, board)) 
        res.append('key_sentence: "{}"\n'.format(original_title + ' ' + ' '.join(summary).replace('\\', '\\\\').replace('"', '\\"')))
        res.append('title: "{}"\n'.format(title.replace('\\', '\\\\').replace('"', '\\"')))
        res.append('date: {} +0800\n'.format(article['Date']))
        # all url in the article
        res.append('post_url: "{}"\n'.format(article_url.replace('\\', '\\\\').replace('"', '')))
        
        res.append('article_url: "{}"\n'.format(';'.join(url['article']).replace('\\', '\\\\').replace('"', '')))
        # all url in the response
        res.append('response_url: "{}"\n'.format(";".join(url['response']).replace('\\', '\\\\').replace('"', '')))
    
    def add_post_images():
        preview_img = ''
        if len(image_url) > 0:
            preview_img = image_url[0]
        res.append('img: {}\n'.format(preview_img))
        # all image url except preview image
        if len(image_url) > 1:
            res.append('all_img: {}\n'.format(';'.join(image_url[1:])))
    
    def get_collections():
        collections = []
        if len(image_url) >= 4:
            collections.append('gallery_post')
        if len(url['article']) > 0:
            for u in YOUTUBE_DOMAIN:
                if url['article'][0].find(u) == 0:
                    collections.append('video_post') 
                    break
        return collections

    def add_post_response_status(): 
        # the response status
        response_count = article['Response_Count']
        res.append('push: {}\n'.format(response_count['push']))
        res.append('boo: {}\n'.format(response_count['boo']))
        res.append('neutral: {}\n'.format(response_count['neutral']))
        res.append('---\n\n')

    def get_video_id(url):
        left = url.find('?v=')
        if left > 0:
            return url[left+3:]
        return url[url.rfind('/') + 1:]
        
    def get_video_embed_url(url):
        video_id = get_video_id(url)
        return YOUTUBE_DOMAIN[0] + '/embed/' + video_id
        
    def append_media():
        if 'video_post' in keywords:
            video_embed_url = get_video_embed_url(url['article'][0])
            res.append("<iframe width='600' height='330' src='%s'></iframe>" % video_embed_url)
        else:
            res.append('<img src="{}" alt="image">\n'.format(image_url[0]))

    def add_post_content():
        # preview image
        if len(image_url) > 0 :
            res.append('<figure>\n')
            append_media()
            res.append('<figcaption>\n')
            res.append('{}\n'.format(article['Title']))
            res.append('</figcaption>\n')
            res.append('</figure>\n\n')
            res.append('\n\n')
        # content
        res.append(paragraph.replace('，，', '，').replace('\n', '\n\n'))
        res.append(u'<a id="post_link" href = "{}">原文連結</a>\n\n'.format(article_url))   

    print('generate post')
    res = []
    add_post_head()
    add_post_images()
    add_post_response_status()
    add_post_content() 
    return ''.join(res)


def journalist(crawl=True, gen_summary=True, response=False, database=True, dry_run=False):
    '''
    Crawl and generate news
    Args:
        response: bool, whether to use the image_url in responses.
        database: bool, whether to use the image_url found in database.
    '''
    boards = get_boards()
    hot_board = get_hot_boards();
    news_generator = News_Generator()
    max_pages = 30 
    board_count = 1

    for cat in boards:
        crawl_board = [ x.strip() for x in boards[cat] ] 

        for board in crawl_board:
            thr = HOT_THRESHOLD if board in hot_board else REGULAR_THRESHOLD
            if crawl:
                print('Crawling board %s %d' % (board, board_count))
                crawler.crawl_past_days(board, datetime.datetime.now(), -1, thr, max_pages, dry_run)

            if gen_summary:
                print('Generating summary board %s %d' % (board, board_count))
                generate_summary(news_generator, cat, board, thr, response, database, dry_run)
            board_count+= 1

def generate_summary(news_generator, cat, board, thr, response, database, dry_run):
    
    def gen_post():
        if articles[j]["summary"]:
            print("has summary")
            res = articles[j]["summary"]
        else:
            if len(titles[j].strip()) == 0 or len(title.strip()) == 0:
                return None
            find_images(title, image_urls, urls[j], database, response) 
            res = generate_post(board, articles[j], summarys[j], keywords[j], responses[j], titles[j], paragraphs[j], article_urls[j], urls[j], image_urls, cat)
            if not dry_run:
                print('Writing news.....')
                insert_summary_keywords_into_db(res, keywords[j], articles[j]['Article_id'])
        return res

    read_database = not dry_run
    write_database = not dry_run
    articles, article_urls, urls, summarys, keywords, responses, titles, paragraphs = news_generator.gen_summary_past_days(board, datetime.datetime.now(), -1, thr, read_database)
    
    for j in range(len(articles)):
        image_urls = []
        tag, title = Filter.get_tag(articles[j]['Title'])
        res = gen_post()
        if res == None: 
            continue
        if write_database:
            write_post_to_file(res, articles[j], article_urls[j])
        else:
            print(res)

def post_date_to_date(date):
    month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    c = date[4:7]
    if c in month:
        m = str(month.index(c) + 1)
        if len(m) < 2: m = '0' + m
        dt = datetime.datetime.strptime(m + date[7:], "%m %d %H:%M:%S %Y")
        return dt.strftime("%Y-%m-%d")
    else: return '2017-11-09'

def write_post_to_file(res, article, article_url):
    dateStr = post_date_to_date(article["Date"])
    article_encode = dateStr + '-' + article_url.replace('https://www.ptt.cc/bbs/', '').replace('/', '').replace('html', '')
    filename = '{}markdown'.format(article_encode)
    path = os.path.join(os.getenv('POSTS'), dateStr)
    if not os.path.exists(path):
        os.makedirs(path)
    f = open(os.path.join(os.getenv('POSTS'), dateStr, filename), 'w', encoding='utf-8')
    print('writing ', filename)
    f.write(res)

def insert_summary_keywords_into_db(summary, keywords, article_id):
    keyword_str = '|' + '|'.join(keywords) + '|'
    insertStr = ("update Post set summary = %s, keywords = %s where article_id = %s")
    data = (summary.encode('utf-8'), keyword_str.encode('utf-8'), article_id)
    print("inserting %s summary.." % article_id)
    dbManager.query(insertStr, data)
    
def clean_summary(sentence):
    '''
    Make summary human-friendly and remove deplicated words
    Args:
        sentence: string, the raw summary
    Return
        cleaned summary
    '''
    remove_words = ['[UNK]']
    sentence = re.sub('\ +', '', sentence)
    sentence_list = sentence.split('.')
    key_words = set(sentence_list)

    seen = set()
    cleaned_list = []
    for w in sentence_list:
        if w not in seen and w not in remove_words:
            seen.add(w)
            cleaned_list.append(w)
        
    cleaned = ''.join(cleaned_list)
    return cleaned

def add_summary(articles, summary_dir):
    '''
    Add the automatic generated summary into posts
    Args:
        articles: list, the articles we used
        summary_dir: string, where we write the summary
    '''
    summary_files = os.listdir(summary_dir)
    for summary_file in summary_files:
        summary_path = os.path.join(summary_dir, summary_file)
        with open(summary_path, 'r') as f:
            summary = f.readlines()[0]
        summary = clean_summary(summary)

        index = int(summary_file.replace('decoded', ''))
        try:
            post_name = articles[index] + 'markdown'
        except:
            continue
        post_path = os.path.join(os.getenv('POSTS'), post_name)
        with open(post_path, 'r') as f:
            content = f.read()
        with open(os.path.join('new_posts', post_name), 'w') as f:
            f.write('AutoTitle: ' + summary + '\n')
            f.write(content)

def sync_boards():
    boards = get_board_lists()
    crawler.sync(boards)
    
if __name__ == '__main__':
    def generate_stub_post(article_id, dry_run=False):
        use_database = False
        image_urls = []
        use_image_database = False
        use_response = False
        articles, article_urls, urls, summarys, keywords, responses, titles, paragraphs = news_generator.gen_single_article(article_id, 20, use_database)
        if len(articles) > 0 and titles[0] != None:
            cat = get_category(articles[0]["Board"])
            find_images(titles[0], image_urls, urls[0], use_image_database, use_response)
            res = generate_post(articles[0]["Board"], articles[0], summarys[0], keywords[0], responses[0], titles[0], paragraphs[0], article_urls[0], urls[0], image_urls, cat)
            if not dry_run:
                print('Writing news.....')
                insert_summary_keywords_into_db(res, keywords[0], articles[0]['Article_id'])
            return res
        else:
            return None 

    if len(sys.argv) == 1:
        start = time.time() 
        journalist(gen_summary=False)
        elapsed = (time.time() - start)
        print("execution time: ", elapsed)
    else:
        type = sys.argv[1]
        if type == 'sync-deleted':
            start = time.time()
            sync_boards()
            elapsed = (time.time() - start)
            print("execution time: ", elapsed)
  
        elif type == 'crawl-single':
            if len(sys.argv) < 4: 
                print('wrong argument')
            else:
                article_id = sys.argv[2]
                board = sys.argv[3]
                if len(sys.argv) == 4:
                    article = crawler.crawl_single(article_id, board) 
                    print('article: %s' % article)
                elif len(sys.argv) == 5:
                    if sys.argv[4] == 'dryrun':
                        article = crawler.crawl_single(article_id, board, False) 
                        print('article: %s' % article)
                
        elif type == 'gen-single':
            print('gen single..')
            news_generator = News_Generator()
            if len(sys.argv) < 4: 
                print('wrong argument')
            else:
                article_id = sys.argv[2]
                board = sys.argv[3]
                if len(sys.argv) == 4:
                    res = generate_stub_post(article_id)
                    print(res)
                elif len(sys.argv) == 5:
                    if sys.argv[4] == 'dryrun':
                        res = generate_stub_post(article_id, True)
                        print(res)

        elif type == 'gen-summary':
            if len(sys.argv) == 2:
                start = time.time()
                journalist(crawl=False)
                elapsed = (time.time() - start)
                print("execution time: ", elapsed)
            elif len(sys.argv) == 3:
                para = sys.argv[2] 
                if para == 'dryrun':
                    print('gen summary dry run..')
                    journalist(crawl=False, dry_run=True)

        elif type == 'crawl':
            if len(sys.argv) < 3:
                print('wrong argument')
            else:
                para = sys.argv[2]
                if para == 'dryrun':
                    print('crawler dryrun..')
                    journalist(gen_summary=False, dry_run=True)
