# vim: set ts=4 sw=4 et: -*- coding: utf-8 -*-
import os
import json
import time
import re
import random
from utils.ptt_filter import ArticleFilter
from utils.analyzer import Analyzer
from utils.db.mysql_connector import DBManager
from datetime import datetime, timedelta

class Template:
    ''' Handle the template '''
    def __init__(self):
        self.template_path = os.getenv('TEMPLATE')
        self.load_template()
        self.tag_mapping = {'問卦' : 'ask', '爆卦' : 'explode', '回覆': 'reply'}
        self.date_pattern = '\{date\}'
        self.time_pattern = '\{time\}'
        self.title_pattern = '\{title\}' 
        self.author_pattern = '\{author\}' 
        self.board_pattern = '\{board\}'
        self.summary_pattern = '\{summary_[0-9]*?\}' # may have sumamry_1, sumamry_2...
        self.comment_pattern = '\{comment_[0-9]*?\}' # may have comment_1, comment_2
        self.comment_special_pattern = '\{comment_special_[0-9]*?\}' # may have comment_special_1 .....
        self.comment_by_pattern = ('\{comment_by_[0-9]*?\}', 'comment_summary') # may have comment_by_1, comment_by_2...
    
    def load_template(self):
        '''
        load all templates
        '''
        types = [t for t in os.listdir(self.template_path) if not t.startswith('.')]
        self.all_templates = {}
        for t in types:
            self.all_templates[t] = []
            template_path = os.path.join(self.template_path, t)
            template_names = [name for name in os.listdir(template_path) if not name.startswith('.')]
            for template_name in template_names:
                with open(os.path.join(template_path, template_name), 'r', encoding='utf-8') as f:
                    self.all_templates[t].append(json.load(f))

    def get_template(self, t_type, max_summary, max_response):
        '''
        get the proper template
        Args:
            t_type: string, tag of the article
            max_sumamry: integer, the maximum summary num
            max_responses: integer, the maximum responses num
        Return:
            template class
        '''
        if t_type not in self.tag_mapping.keys():
            t_type = 'wildcard'
        else:
            t_type = self.tag_mapping[t_type]

        candidates = []
        for template in self.all_templates[t_type]:
            if template['summary_num'] <= max_summary and template['comment_num'] <= max_response:
                candidates.append(template)

        self.history = {} # remember the used tag
        if len(candidates) > 0:
            # print("picking template..")
            chosen = random.choice(candidates)
        else:
            chosen = None
        #print(chosen)
        return chosen

    def process_template(self, sentence, date, time, title, author, board, summary, responses):
        '''
        fill the slots in sentence
        Args:
            sentence: string, the template sentence
            date, time, title, author, board, summay, responses: string
        Return:
            filled sentence
        '''
        # print('sentence: %s' % sentence)
        date_match = re.findall(self.date_pattern, sentence)
        time_match = re.findall(self.time_pattern, sentence)
        title_match = re.findall(self.title_pattern, sentence)
        author_match = re.findall(self.author_pattern, sentence)
        board_match = re.findall(self.board_pattern, sentence)
        summary_match = re.findall(self.summary_pattern, sentence)
        comment_match = re.findall(self.comment_pattern, sentence)
        comment_special_match = re.findall(self.comment_special_pattern, sentence)
        comment_by_match = re.findall(self.comment_by_pattern[0], sentence)
        if len(date_match) > 0:
            sentence = sentence.replace(date_match[0], date) 
        if len(time_match) > 0:
            sentence = sentence.replace(time_match[0], time)
        if len(title_match) > 0:
            sentence = sentence.replace(title_match[0], title)
        if len(author_match) > 0:
            sentence = sentence.replace(author_match[0], author)
        if len(board_match) > 0:
            sentence = sentence.replace(board_match[0], board)
        if len(summary_match) > 0:
            for m in summary_match:
                if m not in self.history.keys():
                    self.history[m] = summary.pop(0)
                sentence = sentence.replace(m, self.history[m])
        if len(comment_match) > 0:
            for m in comment_match:
                if m not in self.history.keys() and len(responses) > 0:
                    self.history[m] = responses.pop(0)['content']
                sentence = sentence.replace(m, self.history[m])
        if len(comment_special_match) > 0:
            for m in comment_special_match:
                if m not in self.history.keys():
                    self.history[m] = responses.pop()['content']
                sentence = sentence.replace(m, self.history[m])
        if len(comment_by_match) > 0:
            for m in comment_by_match:
                if m not in self.history.keys():
                    response = responses.pop()
                    self.history[m] = (response['author'], response['content'])
                sentence = sentence.replace(m, self.history[m][0])
                sentence = sentence.replace(m.replace('comment_by', self.comment_by_pattern[1]), self.history[m][1])
        return sentence
            
        
    def fill_template(self, template, date, time, title, author, board, summary, responses):
        '''
        Fill the template
        Args:
            template: dict, the template
            date, time, title, author, board: string, the necessary information
            sumamry, responses: list, summary of article and responses
        Return:
            news_title: string, title of the generated news
            news_paragraph: string, paragraphhs of the generated news
        '''
        template_title, template_paragraphs = template['title'], template['paragraphs']
        news_title = self.process_template(template_title, date, time, title, author, board, summary, responses)
        news_paragraph = ''
        for template_paragraph in template_paragraphs:
            news_paragraph += self.process_template(template_paragraph, date, time, title, author, board, summary, responses)
            news_paragraph += '\n'
        #print(news_title, news_paragraph)
        return news_title, news_paragraph


class News_Generator:
    ''' Generate the news '''
    def __init__(self):
        '''
        filter: clean the text and use some magic regex
        remove_tag: skip these tags when generating news
        data_path: path of data directory
        analyzer: analyze the crawled article
        template: handle the taiwan journalist template
        '''
        self.dbManager = DBManager()
        self.filter = ArticleFilter()
        self.remove_tag = ['公告', '協尋', '新聞']
        self.data_path = os.path.join(os.getenv("DATA"), "raw" ) 
        self.analyzer = Analyzer()
        self.template = Template()

    def gen_summary_past_days(self, board, endTime, days, thr, read_database):
        daysAgo = endTime - timedelta(days = -days)
        ys = daysAgo.strftime("%m/%d/%Y")
        startTime = datetime.strptime(ys, "%m/%d/%Y")
        return self.find_and_generate(board, thr, startTime, read_database)
    
    def find_and_generate(self, board, thr, startTime, read_database):
        '''
        Find the crawled ptt page and generate news
        Args:
            board: string, specify the ptt board
            thr: integer, the threshold of 'push'
            index: integer, specify the page
        Return:
            used_article: list, articles used to generate news
            article_urls: list, the urls linked to original article
            urls: list, the urls which appears in the article
            sumamry, response: list, the summary and key responses of articles
            titles: list, the title of news
            paragraphs: list, the paragraphs of news
        '''
        print('Generate news from', board)
        used_article, article_urls, urls, titles, paragraphs = [], [], [], [], []
        summarys, keywords, responses = [], [], []

        articles = self.get_articles(board, startTime)
        print('articles to gen: ', len(articles))
        for article in articles:
            self.add_news(article, thr, used_article, titles, paragraphs, urls, article_urls, summarys, keywords, responses, read_database)
        return used_article, article_urls, urls, summarys, keywords, responses, titles, paragraphs

    def add_news(self, article, thr, used_article, titles, paragraphs, urls, article_urls, summarys, keywords, responses, read_database):
        try:
            push_num = article['Response_Count']['push']
        except:
            push_num = 0

        if push_num > thr:
            if article["summary"] and read_database:
                print('has summary: %s' % article["summary"])
                article_url = 'https://www.ptt.cc/bbs/' + article['Board'] + '/' + article['Article_id'] + '.html'
                url = response = summary = title = paragraph = keyword = None
                has_genereated = True
            else:
                article['summary'] = None
                print("generating %s %s.." % (article["Article_id"], article["Title"]))
                has_genereated = False
                article_url, url, summary, response, title, paragraph, keyword = self.generate_news(article)
    
            if summary or has_genereated:
                used_article.append(article)
                titles.append(title)
                paragraphs.append(paragraph)
                urls.append(url)
                article_urls.append(article_url)
                summarys.append(summary)
                responses.append(response)
                keywords.append(keyword)
        else:
            print('pushnum too low: %d thr %d' % (push_num, thr))
    
    def gen_single_article(self, article_id, thr, read_database):
        used_article, article_urls, urls, titles, paragraphs, summarys, keywords, responses = [], [], [], [], [], [], [], []
        articles = self.get_article_by_id(article_id)
        for article in articles:
            self.add_news(article, thr, used_article, titles, paragraphs, urls, article_urls, summarys, keywords, responses, read_database)
        return used_article, article_urls, urls, summarys, keywords, responses, titles, paragraphs

    def get_article_by_id(self, article_id):
        selectStr = ("select content, summary, keywords from Post where article_id = %s")
        keys = (article_id, )
        # print(selectStr % keys)
        rows = self.dbManager.select(selectStr, keys)
        articles = []
        for row in rows:
            filename = row[0]
            summary = row[1]
            keywords = row[2]
            if os.path.exists(filename):
                with open(filename, 'r', encoding='utf-8') as f:
                    post = json.load(f)
                    post["summary"] = summary
                    if keywords:
                        keywords = keywords.split('|')[1:-1]
                    post["keywords"] = keywords
                    articles.append(post)
            else:
                print('No such file!')
	
        return articles

    def time_mapper(self, time):
        '''
        Map the time into modern Mandarin
        Args:
            time: string
        Return:
            the Mandarin form of time
        '''
        splited_time = list(map(int, time.split(':')))
        return '{}點{}分{}秒'.format(splited_time[0],splited_time[1],splited_time[2])

    def date_mapper(self, date):
        '''
        Map the date into modern Mandarin
        Args:
            date: string
        Return:
            the Mandarin form of date
        '''
        splited_date = date.split()
        month = {'Jan':'1', 'Feb':'2','Mar':'3',
                'Apr':'4', 'May':'5', 'Jun':'6',
                'Jul':'7', 'Aug':'8', 'Sep':'9',
                'Oct':'10', 'Nov':'11', 'Dec':'12'}
        return '{}年{}月{}日'.format(splited_date[3],
                                    month[splited_date[1]],
                                    splited_date[2])

    def generate_news(self, article):
        '''
        Generate the news from article
        Args:
            article: dict, the crawled article
        Return:
            article_url: string, the url linked to article
            url: string, the urls which appears in the article
            all_sumamry, responses: list, the summary and key responses of articles
            title: string, the title of news
            paragraph: string, the paragraphs of news
        '''
        # Filter out some special article
        if article['Title'].startswith('Fw') or self.analyzer.check_article(article['Content']):
            print('Fw or on content, ignored')
            return None, None, None, None, None, None, None

        # Split the tag and title
        tag, title = self.filter.get_tag(article['Title'])
        if tag in self.remove_tag:
            print('Tag {} is ignored!'.format(tag))
            return None, None, None, None, None, None, None
        if article['Title'].startswith('Re'):
            tag = '回覆'

        # Get the template
        max_summary = self.analyzer.get_content_len(article['Content'])
        responses = self.analyzer.find_useful_response(article['Responses'], 5)
        max_response = len(responses)
        # print('max summary:{}, max response:{}'.format(max_summary, max_response))
        if tag != None:
            template = self.template.get_template(tag, max_summary, max_response)
        else:
            template = self.template.get_template(tag, max_summary, max_response)
        if template == None:
            print('No template!')
            return None, None, None, None, None, None, None
        
        # Clean author id
        author = article['Author']
        author = re.sub('\(.*?\)', '', author).strip()

        # Deal with urls
        board = article['Board']
        article_url = 'https://www.ptt.cc/bbs/' + article['Board'] + '/' + article['Article_id'] + '.html'
        url = {'article': self.analyzer.get_url(article['Content']), 'response':self.analyzer.get_response_url(article['Responses'])}
        #print('url', url)

        # Extract summary and response
        # print('gen summary..', article['Article_id'])
        print('remove signature..')
        content = self.remove_signature(article['Content'])
        keywords, summarys = self.analyzer.find_summary(title, content, template['summary_num'])
        print('keywords: %s' % keywords)
        #print('gen response.. comment num %d' % template['comment_num'])
        #print(responses)

        # Deal with the article date
        all_date = article['Date'].split()
        if len(all_date) < 5:
            # When the crawler failed, give special value
            article_time = '11:26:26'
            date = 'Thu Jul 20 2017'
        else:
            article_time = all_date.pop(3)
            date = ' '.join(all_date)
        article_time, date = self.time_mapper(article_time), self.date_mapper(date)
        
        # Fill the template
        print('fill template..')
        title, paragraph = self.template.fill_template(template, date, article_time, title, author, board, summarys, responses)

        # Maybe we want the pure summary and key responses
        clean_content = self.analyzer.filter.clean_content(content=content)
        print('extract key sentences..')
        startTime = time.time()
        key_summary = self.analyzer.extract_key_sentences(clean_content, sort_by_index=True, num = 20)
        print("time: ", time.time() - startTime)
        all_summary = [s[2] for s in key_summary]
        return article_url, url, all_summary, responses, title, paragraph, keywords

    def remove_signature(self, content):
        # print('content: %s' % content)
        end = len(content)
        start = None
        lines = 0
        for i in range(end-1, 1, -1):
            if content[i:i+2] == '--':
                end = i
                for i in range(end-2, 1, -1):
                    if content[i:i+3] == '\n--' or content[i:i+6] == '\n-----':
                        start = i+1
                        break
                    if content[i] == '\n': lines+= 1
                break

        print('lines: %d' % lines)
        if start and lines < 8:
            print('signature(%d %d): %s' % (start, end, content[start:end+2]))
            return content[0:start] + content[end+2:]

        print('no signature')
        return content
 
    def get_articles(self, board, time):
        '''
        Get the crawled page by the modified time
        Args:
            board: string, specify the board
            index: get index(st) page from the directory
        Return:
            articles
        '''
        def get_pagenum(filename):
            return int(re.findall(r'\d+', filename)[0])
        def get_modified(filename):
            return os.path.getctime(filename)
	
        selectStr = ("select content, summary, keywords from Post where board = %s and post_date > %s and status = 'regular' order by post_date asc")
        keys = (board, time.strftime("%Y-%m-%d %H:%M:%S"))
        # print(selectStr % keys)
        rows = self.dbManager.select(selectStr, keys)
        articles = []
        for row in rows:
            filename = row[0]
            summary = row[1]
            keywords = row[2]
            print("filename: ", filename)
            if os.path.exists(filename):
                with open(filename, 'r', encoding='utf-8') as f:
                    post = json.load(f)
                    post["summary"] = summary
                    post["keywords"] = keywords
                    articles.append(post)
            else:
                print('No such file!')
	
        return articles

if __name__ == '__main__':
    if len(sys.argv) == 3:
        news_generator = News_Generator()
        article_id = sys.argv[1] 
        articles, article_urls, urls, summarys, responses, titles, paragraphs = news_generator.gen_single_article(article_id)
        for j in range(len(articles)):
            print("title: %s\n summary: %s" % (titles[j], summarys[j]))

    else:
        news_generator = News_Generator()
        yesterday = datetime.now() - timedelta(days = 1)
        yesterdayS = yesterday.strftime("%m/%d/%Y")
        startTime = datetime.strptime(yesterdayS, "%m/%d/%Y")
        news_generator.get_articles("Gossiping", startTime) 
