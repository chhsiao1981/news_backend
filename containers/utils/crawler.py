# vim: set ts=4 sw=4 et: -*- coding: utf-8 -*-
# -*- coding: utf-8 -*- 
import re
import sys
import json
import requests
import argparse
import time
import os
import codecs
from bs4 import BeautifulSoup
from six import u
from datetime import datetime, timedelta
from utils.db.mysql_connector import DBManager
from shutil import copyfile

__version__ = '1.0'

# if python 2, disable verify flag in requests.get()
VERIFY = True
if sys.version_info[0] < 3:
    VERIFY = False
    requests.packages.urllib3.disable_warnings()

PTT_URL = 'https://www.ptt.cc'

class PttWebCrawler(object):
    """docstring for PttWebCrawler"""
    UPDATE_THRESHOLD = 20
    def __init__(self, dbManager):
        PttWebCrawler.dbManager = dbManager

    @staticmethod
    def dateToMySQLDateStr(date):
        if date:
            return date.strftime("%Y-%m-%d %H:%M:%S")
        return None
    
    @staticmethod
    def postDateStrToDate(date):
        # 'Tue Oct 24 23:42:06 2017'
        month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        c = date[4:7]
        if c in month:
            m = str(month.index(c) + 1)
            if len(m) < 2: m = '0' + m
            return datetime.strptime(m + date[7:], "%m %d %H:%M:%S %Y")
        else: return None

    def inTime(self, dateStr, startTime, endTime):
        date = self.postDateStrToDate(dateStr)
        if date:
            print('date start end: ', date, startTime, endTime)
            return date >= startTime and date <= endTime
        else: return False
   
    def crawlArticle(self, article_id, board):
        link = PTT_URL + '/bbs/' + board + '/' + article_id + '.html'
        article = self.crawl(link, article_id, board)
               
    def crawl_past_days(self, board, endTime, days, thr, maxPages, dryRun=False):
        daysAgo = endTime - timedelta(days = -days)
        ys = daysAgo.strftime("%m/%d/%Y")
        startTime = datetime.strptime(ys, "%m/%d/%Y")
        return self.crawl_period(board, startTime, endTime, thr, maxPages, dryRun)

    def crawl_period(self, board, startTime, endTime, thr, maxPages, dryRun=False):
        lastPage = self.get_last_page(board)
        res = []
        for i in range(lastPage, max(1, lastPage-maxPages), -1):
            print("crawling page ", i)
            result = self.crawl_page(board, i, startTime, thr, dryRun) 
            if result["inTime"]:
                articles = result["articles"]
                arr = []
                for article in articles:
                    if self.inTime(article['Date'], startTime, endTime): 
                        # print("in time")
                        arr.append(article)    
                res += arr
                time.sleep(0.1)
            else: break
      
        print("num crawled: ", len(res))
        return res

    def crawl_single(self, article_id, board, use_database=True):
        link = PTT_URL + '/bbs/' + board + '/' + article_id + '.html'
        article = self.crawl(link, article_id, board)
        row = self.get_row(article_id)
        article_date = PttWebCrawler.postDateStrToDate(article["Date"])
        if 'error' not in article:
            if use_database:
                self.save_file_and_db(article, article_date, article_id, board, row)
            print(article["Title"])
        return article
    
    def save_file_and_db(self, article, article_date, article_id, board, row):
        filepath = self.save_file(article_date, board + '_' + article_id, article, 'w')
        PttWebCrawler.insert_into_db(article, filepath, row)

    def crawl_page(self, board, index, startTime, thr, dryRun=False):
        ''' hack: get article date without year, because this is the 
            only info we got
            ex: 11/09 -> 2017/11/09 '''
        def get_article_date_without_year(div):
            date_str = get_post_date_from_post_list(div)
            year = startTime.strftime("%Y")
            date = datetime.now()
            if len(date_str) > 0 and date_str[0] == ' ':
                date_str = '0' + date_str[1:]
            _date = datetime.strptime(date_str + '/' + year, "%m/%d/%Y")
            if _date > date:                    # from 12/31 to 1/1 
                return _date - timedelta(days=365) 
            else: 
                return _date 
            
        def crawl_post_list():
            return requests.get(
                url=PTT_URL + '/bbs/' + board + '/index' + str(index) + '.html',
                cookies={'over18': '1'}, verify=VERIFY,
                timeout=3
            )
        
        def get_push_num(div):
            push_div = div.findAll("div", "nrec")[0].find('span')
            push_num = '0'
            if push_div:
                push_num = push_div.string
            return push_num

        def get_post_date_from_post_list(div):
            return str(div.findAll("div", { "class" : "date" })[0].string)
        def get_href(div):
            return div.find('a')['href']

        def get_link(div):
            href = get_href(div)
            link = PTT_URL + href
            return href, link
    
        def crawl_article_if_needed(article_id, article_date, link, push_num, row):
            # print('bb')
            if row == None or self.need_for_update(row, push_num):
                print('crawling ' + link + '...')
                article = self.crawl(link, article_id, board)
                if 'error' not in article:
                    if not dryRun:
                        self.save_file_and_db(article, article_date, article_id, board, row)
                    print(article["Title"])
                    articles.append(article)
            else:
                print("crawled")

        def fetch_article(div):
            push_num = get_push_num(div) 
            print('(0) push num %s' % push_num) 
            article_date = get_article_date_without_year(div)
            if article_date >= startTime:
                inTime[0] = True
            # print("article date: ", article_date, article_date >= startTime) 
            if push_num.isdigit() and int(push_num) > thr or push_num == u'爆':
                print('push num ', push_num)
                try:
                    href, link = get_link(div)
                    article_id = re.sub('\.html', '', href.split('/')[-1])
                    row = self.get_row(article_id) if not dryRun else None
                    crawl_article_if_needed(article_id, article_date, link, push_num, row)
                except Exception as inst:
                    print(str(inst))
                    return

        def fetch_article_in_divs():
            for div in reversed(divs):
                if has_separator[0]: 
                    if div.has_attr('class') and div['class'][0] == 'r-list-sep':
                        has_separator[0] = False
                    continue
                if not div.has_attr('class') or not div['class'][0] == 'r-ent': continue
                try:
                    print('getting article..')
                    fetch_article(div)
                except Exception as inst:
                    # print(str(inst))
                    pass

        def get_divs():
            return soup.find("div", "r-list-container").find_all("div", recursive=False)
        
        def check_has_separator():
            return soup.find("div", "r-list-sep") != None

        resp = {}
        try:
            resp = crawl_post_list()
        except Exception as e:
            print('Exception: {}'.format(str(e)))
            return {}

        if resp.status_code != 200:
            print('invalid url:', resp.url)
            return {}
        else:
            articles = []
            inTime = [False]
            has_separator = [False]
            soup = BeautifulSoup(resp.text, 'html.parser')
            divs = get_divs()
            has_separator[0] = check_has_separator()
            fetch_article_in_divs() 
            return {"inTime": inTime[0], "articles": articles}
        
    def get_row(self, article_id):
        print("checking has crawl..")
        row = PttWebCrawler.dbManager.selectByKey('select comment_sum from Post where article_id = %s', article_id)
        return row

    def need_for_update(self, row, push_num):
        return row == None or (row[0] < 100 and (not push_num.isdigit() or int(push_num) >= row[0] + PttWebCrawler.UPDATE_THRESHOLD))

    @staticmethod
    def fetch(link, article_id, board):
        resp = None
        try:
            resp = requests.get(url=link, cookies={'over18': '1'}, verify=VERIFY, timeout=3)
        except Exception as e:
            print("Exception occured: {}".format(str(e)))
        else:
            if resp.status_code != 200:
                print('invalid url:', resp.url)
                return {"error": "invalid url"}
        return {}
        
    @staticmethod
    def crawl(link, article_id, board):
        # print('Loading article:', article_id)
        resp = requests.get(url=link, cookies={'over18': '1'}, verify=VERIFY, timeout=3)
        if resp.status_code != 200:
            print('invalid url:', resp.url)
            return {"error": "invalid url"}
        
        soup = BeautifulSoup(resp.text, 'html.parser')
        main_content = soup.find(id="main-content")
        metas = main_content.select('div.article-metaline')
        author = ''
        title = ''
        date = ''
        if metas:
            author = metas[0].select('span.article-meta-value')[0].string if metas[0].select('span.article-meta-value')[0] else author
            title = metas[1].select('span.article-meta-value')[0].string if metas[1].select('span.article-meta-value')[0] else title
            date = metas[2].select('span.article-meta-value')[0].string if metas[2].select('span.article-meta-value')[0] else date
            if title == None:
                title = metas[1].select('span.article-meta-value')[0].contents[0] if metas[1].select('span.article-meta-value')[0] else title
            if author == None:
                author = metas[0].select('span.article-meta-value')[0].contents[0] if metas[0].select('span.article-meta-value')[0] else author
            # remove meta nodes
            for meta in metas:
                meta.extract()
            for meta in main_content.select('div.article-metaline-right'):
                meta.extract()

        # remove and keep push nodes
        pushes = main_content.find_all('div', class_='push')
        for push in pushes:
            push.extract()

        try:
            ip = main_content.find(text=re.compile(u'※ 發信站:'))
            ip = re.search('[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*', ip).group()
        except:
            ip = "None"

        # 移除 '※ 發信站:' (starts with u'\u203b'), '◆ From:' (starts with u'\u25c6'), 空行及多餘空白
        # 保留英數字, 中文及中文標點, 網址, 部分特殊符號
        filtered = [ v for v in main_content.stripped_strings if v[0] not in [u'※', u'◆']]
        expr = re.compile(u(r'[^\u4e00-\u9fa5\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\s\w:/-_.?~%()]'))
        #for i in range(len(filtered)):
        #    filtered[i] = re.sub(expr, '', filtered[i])
        # print(filtered)
        filtered = [_f for _f in filtered if _f]  # remove empty strings
        filtered = [x for x in filtered if article_id not in x]  # remove last line containing the url of the article
        content = ' '.join(filtered)
        #content = re.sub(r'(\s)+', ' ', content)
        # push messages
        p, b, n = 0, 0, 0
        messages = []
        for push in pushes:
            if not push.find('span', 'push-tag'):
                continue
            push_tag = push.find('span', 'push-tag').string.strip(' \t\n\r')
            push_userid = push.find('span', 'push-userid').string.strip(' \t\n\r')
            # if find is None: find().strings -> list -> ' '.join; else the current way
            push_content = push.find('span', 'push-content').strings
            push_content = ' '.join(push_content)[1:].strip(' \t\n\r')  # remove ':'
            push_ipdatetime = push.find('span', 'push-ipdatetime').string.strip(' \t\n\r')
            messages.append( {'Vote': push_tag, 'User': push_userid, 'Content': push_content, 'Ipdatetime': push_ipdatetime} )
            if push_tag == u'推':
                p += 1
            elif push_tag == u'噓':
                b += 1
            else:
                n += 1

        # count: 推噓文相抵後的數量; all: 推文總數
        message_count = {'all': p+b+n, 'count': p-b, 'push': p, 'boo': b, "neutral": n}

        # print 'mscounts', message_count

        # json data
        data = {
            'Board': board,
            'Article_id': article_id,
            'Title': title,
            'Author': author,
            'Date': date,
            'Content': content,
            'Ip': ip,
            'Response_Count': message_count,
            'Responses': messages
        }
        #print('original:', d)
        return data

    @staticmethod
    def get_last_page(board):
        content = requests.get(
            url= 'https://www.ptt.cc/bbs/' + board + '/index.html',
            cookies={'over18': '1'},
            timeout=3
        ).content.decode('utf-8')
        first_page = re.search(r'href="/bbs/' + board + '/index(\d+).html">&lsaquo;', content)
        if first_page is None:
            return 1
        return int(first_page.group(1)) + 1

    @staticmethod
    def save_file(postDate, filename, article, mode):
        data = json.dumps(article, sort_keys=True, ensure_ascii=False)
        date = postDate.strftime("%Y/%m/%d")	
        directory = '/crawled/' + date
        if not os.path.exists(directory):
            os.makedirs(directory)
        filename = os.path.join(directory, filename)
        with codecs.open(filename, mode, encoding='utf-8') as f:
            # print("writing static file..")
            f.write(data)
        return filename
    
    @staticmethod
    def insert_into_db(article, filepath, row):
        print("inserting into DB..")
        ctime = datetime.now()
        if row:
            # print('delete row..')
            deleteStr = ("delete from Post where article_id = %s")
            data = (article["Article_id"], )
            PttWebCrawler.dbManager.query(deleteStr, data)

        queryStr = ("insert into Post (article_id, board, title, post_date, comment_sum, metadata, content, status, ctime) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)") 
        postDate = PttWebCrawler.postDateStrToDate(article["Date"])
        dateStr = PttWebCrawler.dateToMySQLDateStr(postDate)
        title = str(article["Title"])
        author = str(article["Author"])
        metaData = {
            "author": author,
            "comment_count": article["Response_Count"]
        }
        metaBlob = json.dumps(metaData, sort_keys=True, ensure_ascii=False)
        print('saving db entry..')
        data = (article["Article_id"], article["Board"], title, dateStr, article["Response_Count"]["count"], metaBlob, filepath, "regular", ctime)
        # print(queryStr, data)
        PttWebCrawler.dbManager.query(queryStr, data)

    @staticmethod
    def get():
        with codecs.open(filename, mode, encoding='utf-8') as f:
            j = json.load(f)
            print(f)

    def get_article_past_days(self, board, startTime):
        queryStr = ("select article_id, board, post_date from Post where post_date > %s and board = %s")
        print(queryStr % (startTime, board))
        t = PttWebCrawler.dateToMySQLDateStr(startTime)
        data = (t, board )
        posts = PttWebCrawler.dbManager.select(queryStr, data)
        return posts

    def update_post_status(self, article_id, status):
        queryStr = ("update Post set status = %s where article_id = %s")
        data = (status, article_id)
        PttWebCrawler.dbManager.query(queryStr, data)

    def is_deleted(self, board, article_id):
        link = PTT_URL + '/bbs/' + board + '/' + article_id + '.html'
        article = self.fetch(link, article_id, board)
        if 'error' in article:  
            return True
        '''soup = article["content"]
        main_content = soup.find("div", "main-container")
        if main_content:
            divs = main_content.select('div.bbs-content')
            if divs and divs[0].string.find('404 - Not Found') == 0:
                return True'''
        return False 
        
    def delete_post(self, post):
        dateStr = post[2].strftime("%Y-%m-%d")
        filename = dateStr + '-' + post[1] + post[0] + '.markdown'
        newName = dateStr + '-' + post[1] + post[0] + '-DELETED.markdown'
        path = os.path.join(os.getenv('POSTS'), dateStr, filename)
        newPath = os.path.join(os.getenv('POSTS'), dateStr, newName)
        deleteFolder = os.path.join(os.getenv('POSTS'), 'deleting')
        deleteFolderMobile = os.path.join(os.getenv('POSTS'), 'deleting-mobile')
        copyPath = os.path.join(deleteFolder, filename)
        copyPathMobile = os.path.join(deleteFolderMobile, filename)
        if not os.path.exists(deleteFolder):
            os.mkdir(deleteFolder)
        if not os.path.exists(deleteFolderMobile):
            os.mkdir(deleteFolderMobile)
        if os.path.exists(path):
            # print('deleting %s..' % filename)
            # os.remove(path)
            print('moving file from {} to {} and {}'.format(path, copyPath, copyPathMobile))
            copyfile(path, copyPath)
            copyfile(path, copyPathMobile)
            os.rename(path, newPath)
        else:
            print('files does not exist, skip: {}'.format(path))
        
    def sync_past_days(self, board, endTime, days):
        daysAgo = endTime - timedelta(days = days)
        ys = daysAgo.strftime("%m/%d/%Y")
        startTime = datetime.strptime(ys, "%m/%d/%Y")   
        articles = self.get_article_past_days(board, startTime)
        print('article to check: %d' % len(articles))
        count = 1
        for p in articles:
            print('%d' % count, end=' ', flush=True)
            count+= 1
            if self.is_deleted(board, p[0]):
                print('deleting %s %s...' % (board, p[0]))
                self.update_post_status(p[0], 'deleted') 
                self.delete_post(p)
        print('')
     
    def sync(self, boards):
       for board in boards:
            print('checking %s...' % board)
            self.sync_past_days(board, datetime.now(), 1)
     

if __name__ == '__main__':
    start = time.time()
    dbManager = DBManager()
    if len(sys.argv) == 3:
        c = PttWebCrawler(dbManager)
        c.crawlArticle(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 4:
        type = sys.argv[1]
        if type == 'test_delete':
            id, board = sys.argv[2], sys.argv[3]
            c = PttWebCrawler(dbManager)
            print(c.is_deleted(board, id))
            
    else:
        c = PttWebCrawler(dbManager)
        '''yesterday = datetime.now() - timedelta(days = 1)
        yesterdayS = yesterday.strftime("%m/%d/%Y")
        startTime = datetime.strptime(yesterdayS, "%m/%d/%Y") 
        endTime = datetime.now()
        c.crawl_period('Gossiping', startTime, endTime, 50)'''
        c.crawl_past_days('Gossiping', datetime.now(), -1, 50, True)
        elapsed = (time.time() - start)
        print("execution time: ", elapsed)
