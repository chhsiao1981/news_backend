# vim: set ts=4 sw=4 et: -*- coding: utf-8 -*-
import os
import sys
import json
import random
import re
import math
import urllib
import requests
import copy
import metadata_parser
from utils.textrank4zh import TextRank4Keyword, TextRank4Sentence
from utils.ptt_filter import ArticleFilter
from utils.db.mysql_connector import DBManager

dict_path = os.path.join(os.getenv("JIEBA_DATA"), "dict.txt.big") 
stopword_path = os.path.join(os.getenv("DATA"), "stopwords/chinese_sw.txt" )
MAX_KEYWORD_NUM = 3
dbManager = DBManager(use_face_db=True)

class Analyzer:
    ''' Analyze the ptt article '''
    def __init__(self):
        '''
        tr4s: extract keysentences
        tr4w: extract keywords
        filter: clean the text and use some magic regex
        '''
        self.tr4s = TextRank4Sentence(stop_words_file = stopword_path)
        self.tr4w = TextRank4Keyword(stop_words_file= stopword_path)
        self.filter = ArticleFilter()

    def get_url(self, content):
        ''' 
        get the url from content
        Args: 
            url: string
        Return: 
            list of string
        '''
        return self.filter.get_url(content)

    def get_content_len(self, content):
        '''
        get the length of content
        Args: 
            content: string
        Return: 
            integer
        '''
        clean_content = self.filter.clean_content(content=content)
        # print('content: %s %d' % (clean_content, len(clean_content.split('\n'))))
        return len(clean_content.split('\n'))
    
    def get_response_num(self, responses):
        '''
        get the number of useful responses
        Args: 
            responses: list of dict
        Return: 
            integer
        '''
        clean_responses = self.filter.clean_responses(responses, self.filter.stopwords)
        print('Response: {}, Clean response: {}'.format(len(responses), len(clean_responses)))
        return len(clean_responses)

    def get_response_url(self, responses):
        '''
        get all url from the responses
        Args: 
            responses: list of dict
        Return: 
            list of string
        '''
        urls = []
        for response in responses:
            content = re.sub('\ +', '//', response['Content'])
            urls += self.filter.get_url(response['Content'])
            urls += self.filter.get_url(content)
        return list(set(urls))

    def check_article(self, content):
        '''
        check whether the article is 'Give P Coin' article
        Args: 
            content: string
        Return: 
            bool
        '''
        pattern = u'[發推人前樓各噓得分]+[ ]*[\d]+[pP]'
        reward = re.findall(pattern, content)
        # print(reward)
        if len(reward) > 0:
            return True
        else:
            return False

    def open_url(self, url):
        '''
        Try to get the image url from the input url
        If the input url is not a image and open graph protocal
        gives nothing, it returns 'None'
        Args: 
            url: string
        Return: 
            string or None
        '''
        def get_type(url):
            try:
                print('opening url')
                with urllib.request.urlopen(url, timeout=3) as response:
                    mime_type = response.info().get_content_type()
                print(mime_type)
                return mime_type.split('/')
            except:
                return [None, None]
        if get_type(url)[0] == 'image':
            return url
        else:
            print('try og')
            try:
                page = metadata_parser.MetadataParser(url=url, requests_timeout=3)
                image_link = page.get_metadata_link('image')
                if image_link != None:
                    #image_url.append(image_link)
                    return image_link
            except:
                return None
    
    def find_summary(self, title, content, summary_num = 5, debug=True):
        '''
        generate the summary from input content
        Args: 
            content: string
            summary_num: integer, how many summary you want
            debug: bool, whether to print the result
        Return:
            list of string
        '''
        clean_content = self.filter.clean_content(content=content)

        # at most extract (content / 5) sentences from content
        max_num = int(len(clean_content.split('\n'))/5) 
        num = max_num if max_num > summary_num * 2 else summary_num * 2
        if len(clean_content) < 150: # if the article is short enough
            num = 1e6
        if num > 20: # if the article is too long
            num = 20

        key_sentences = self.extract_key_sentences(clean_content, sort_by_index=True, num = num)
        key_sentences = [x[2] for x in key_sentences]
        keywords = self.extract_keywords(title + ' ' + clean_content)
        # print('keyword(1): %s' % ' '.join(keywords))
        if debug:
            #print('Original content:', content.encode('utf8'))
            #print('Cleaned content:', clean_content.encode('utf8'))
            print('Length of cleaned content:', len(clean_content))
            #print('Key sentences:', key_sentences)
            print('Num of key sentences', len(key_sentences))
        
        '''
        Divide the keysentences into $summary_num part
        '''
        summarys = []
        summary_len = [1 for _ in range(summary_num)] # each part deserve 1 sentence
        rest = len(key_sentences) - summary_num # num of remain sentences
        if rest > summary_num:
            # equally distribute the key sentences to all part
            factor = int(rest/summary_num)
            rest = rest - (factor * summary_num)
            summary_len = [x + factor for x in summary_len]
        rest_count = 0
        for i in range(len(summary_len)):
            if rest > 0:
                # assign the remain sentences if we have
                summary_len[i] += 1
                rest -= 1
            summarys.append('，'.join(key_sentences[rest_count:rest_count + summary_len[i]]))
            rest_count += summary_len[i]

        return keywords, summarys
    
    def find_useful_response(self, responses, num = 5):
        ''' 
        preserve the original responses
        merge the all responses into one article
        '''
        clean_responses = self.filter.clean_responses(responses, self.filter.stopwords)
        if len(clean_responses) != 0:
            responses = clean_responses

        response_dict = {}
        all_response = ''
        for response in responses:
            all_response += response['Content'].replace(' ', '') + '\n'
            response_dict[response['Content'].replace(' ','').strip()] = response
        
        # run text rank
        key_responses = self.extract_key_sentences(all_response, sort_by_index=False, num=num)
        important_responses = []
        
        # restore the responses
        for r in key_responses:
            if r[2].strip() in response_dict.keys():
                response = response_dict[r[2].strip()]
                author, content = response['User'], response['Content']
                ipdatetime, vote = response['Ipdatetime'], response['Vote']
            else:
                author, content = 'unk', r[2]
                ipdatetime, vote = 'unk', 'unk'
            content = re.sub('\ +', '，', content)
            important_responses.append({'author':author, 'content':content, 'vote':vote, 'ipdatetime':ipdatetime})
        return important_responses
   
    def search_people_name(self, content):
        g_name_to_id = {}
    
        f = open('data/name-ids.json', 'r', encoding='utf-8')
        g_name_to_id = json.load(f, encoding='utf-8')
        g_re_names = re.compile("(" + "|".join(g_name_to_id.keys()) + ")", re.U)

        matched = {}
        count = 0
        for m in g_re_names.finditer(content):
            pname = m.group(1)
            pos = m.start()
            matched[pname] = True
            count += 1 
            if count == MAX_KEYWORD_NUM:
                break
           
        return matched
       
    def extract_keywords(self, content):
        '''
        extract the keywords from content
        Args:
            content: string
        Return:
            key_words: list of string
        '''
        clean_content = self.filter.clean_content(content=content)
        self.tr4w.analyze(text=clean_content, lower=True, window=2)
        keywords = self.search_people_name(clean_content)
        print('person name: %s' % keywords)
        for item in self.tr4w.get_keywords(MAX_KEYWORD_NUM, word_min_len=2):
            #print(item.word, item.weight)
            keywords[item.word] = True
            if len(keywords) >= MAX_KEYWORD_NUM:
                break
        return list(keywords.keys())
        
    def extract_key_sentences(self, content, sort_by_index=False, num = 5):
        '''
        extract keysentences from content
        Args:
            content: string
            sort_by_index: bool, whether sort the output by line index
            num: integer, how many sentences we want
        Return:
            list of information of key_sentences
        '''
        self.tr4s.analyze(text=content, lower=True, source = 'all_filters')
        key_sentences = []
        for item in self.tr4s.get_key_sentences(num=num, sentence_min_len=1):
            key_sentences.append([item.index, item.weight, item.sentence])
            #print(item.index, item.weight, item.sentence) 
        #print('=====')
        def index(x):
            return x[0]
        def weight(x):
            return x[1]

        return sorted(key_sentences, key=index) if sort_by_index else sorted(key_sentences, key=weight)

if __name__ == '__main__':
    analyzer = Analyzer()
    keywords = analyzer.search_people_name(u"""
熱鬧的PTT八卦版，今日出現一篇標題寫著「零秒儀直接宣佈散會」的文章，吸引眾網友紛紛發表意見。

有位名為shinbird的網友回文說，「請幫高調，偽 逐條審查part，賴清德下個禮拜是國民黨的蔣萬安當召委，不會讓這個進度走下去，可是下下禮拜又是林靜儀來當召委，林靜儀委員的質詢題目是什麼，林靜儀委員對於現在...因為林靜儀召委沒有辦法表態，但是這些條文林靜儀委員是可以表態的，勞工是民進黨心裡最軟的一塊，說好要到，國民黨有81席，今天民進黨68席，但後來該作的公聽會加開什麼的，你可以說是當年的民進黨有作為才讓國民黨就範，但那也要國民黨給民進黨面子才行，民進黨比國民黨更超過，王金平當年也可以下令把民進黨立委全部拉下台啊，我過去十多年來一直支持民進黨，你當年可以因為國民黨太爛而投給民進黨，我下次遇到不會打他」。

鄉民控制不住對這個精闢解答的崇拜，積極回覆，其中許多人表示：「KMT不倒，台灣不會好，不然你要投國民黨」、「不然你要投KMT」或是「不投蔡英文但死都不投kmt」，而也有鄉民坦言內心想法：「推最後一段，變成國民黨的民進黨，絕對不投」、「我也是不敢再投DPP，因為這兩年比ＫＭＴ還糟糕，加上」，令人激賞。

您心目中的解答，是什麼呢？
    """)
    print('names: %s' % keywords)
    #news_generator = News_Generator()
    #print(news_generator.find_and_generate())
    #print(news_generator.get_template(t_type='ask'))
