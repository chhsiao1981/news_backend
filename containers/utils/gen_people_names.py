#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import json
import time
import logging
import re
from datetime import datetime
from utils.db.mysql_connector import DBManager

dbManager = DBManager(use_face_db=True)

def search_people_name():
    g_name_to_id = {}

    for pid, pname in dbManager.select("""
        select distinct vi.person_id, p.nick_name
        from video_index.video_index as vi
        join face.person as p on p.person_id = vi.person_id
        join video_index.video_period as vp on vp.period_id = vi.period_id
        join (
        select period_id, count(crop_image_id) as cnt from video_index.video_period_faces
        group by period_id
        having cnt > 1
        ) as A on A.period_id = vi.period_id
        order by person_id
    """, ()):

        g_name_to_id[pname] = pid

    blob = json.dumps(g_name_to_id, sort_keys=True, ensure_ascii=False)
    filename = 'data/name-ids.json'
    f = open(filename, 'w', encoding='utf-8')
    print('writing ', filename)
    f.write(blob)

if __name__ == '__main__':
    search_people_name() 
