#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import json
from datetime import datetime
from utils.db.facedb_connector import DBManager

dbManager = DBManager()

def get_vid(url):
    start = url.find('v=')
    if start < 0:
        return None
    end = url.find('&', start)   
    if end < 0: 
        end = len(url) 
    return url[start+2:end]

def get_videos(pid, name):
    videos = []
    for r in dbManager.select("""
        select  A.source_id,
                A.title, A.description, 
                DATE_FORMAT(CONVERT_TZ(A.publish, '+00:00', @@session.time_zone), GET_FORMAT(DATETIME,'ISO')) as publish, 
                A.cnt, A.uri, MIN(vp.start),
                A.duration, A.thumbnail
        from
        (
            select  vp.source_id, count(vpf.crop_image_id) as cnt,
                rs.uri,
                JSON_UNQUOTE(JSON_EXTRACT(rs.meta, '$.title')) as title, 
                SUBSTRING(JSON_UNQUOTE(JSON_EXTRACT(rs.meta, '$.description')), 1, 200) as description, 
                JSON_UNQUOTE(JSON_EXTRACT(rs.meta, '$.publishedAt')) as publish,
                JSON_EXTRACT(rs.meta, '$.duration') as duration,
                JSON_UNQUOTE(JSON_EXTRACT(rs.meta, '$.thumbnails.default.url')) as thumbnail
            from video_index.video_index as vi
            join video_index.video_period as vp on vp.period_id = vi.period_id
            join resource.source as rs on rs.source_id = vp.source_id
            join video_index.video_period_faces as vpf on vpf.period_id = vi.period_id
            where vi.person_id = %s
            group by vp.source_id
            having cnt > 1
            order by publish desc
            limit %s
        ) as A
        join video_index.video_period as vp on vp.source_id = A.source_id
        join video_index.video_index as vi on vi.period_id = vp.period_id
        where person_id = %s
        group by A.source_id
        order by A.publish desc
    """, (pid, 100, pid)):
        source_id, title, desc, published, cnt, uri, start, dur, tn = r
        # print('pid publish %s %s' % (source_id, published)) 
        url = "{}&t={}s".format(uri, int(start/1000))
        vid = get_vid(url)
        if vid: 
            published = datetime.strptime(published, "%Y-%m-%d %H:%M:%S")
            v = {}
            v['source_id'] = source_id
            v['title'] = title.decode('utf-8')
            v['desc'] = desc.decode('utf-8').replace('\\', '\\\\').replace('"', '\\"')
            v['published_date'] = published
            v['duration'] = dur
            v['category'] = 'video youtube'
            v['url'] = url
            v['video_tn'] = tn.decode('utf-8') 
            v['video_embed_url'] = '//www.youtube.com/embed/{}?start={}'.format(vid, int(start/1000)) 
            # print(url)
            # print(published) 
            videos.append(v)

    print('%s len: %d' % (name, len(videos)))
    return videos

def get_template(path):
    f = open(path, 'r', encoding='utf-8')
    content = f.read()
    print('template: %s' % content)
    return content

startTime = time.time()
with open('data/name-ids.json', 'r', encoding='utf-8') as f:
    posts = {}
    name_id = json.load(f, encoding='utf-8')
    sz = len(name_id)
    cnt = 0.0
    template = get_template('template/video-post.html')
    for name in name_id:
        pid = name_id[name]
        # print('getting post for %s..' % name)
        videos =  get_videos(pid, name)
        for v in videos:
            vid = v['source_id']
            if vid in posts:
                posts[vid]['tag'].append([name, pid])
            else:
                posts[vid] = v
                posts[vid]['tag'] = [[name, pid]]
        cnt+= 1
        print("%f%% done" % ((cnt / sz) * 100) )
    print('generating post..')
    for vid in posts:
        t = template
        print('gen %d...' % vid)
        post = posts[vid]
        tags = post['tag']
        post_tags = []
        for tag in tags:
            post_tags.append(tag[0]) 
            #print('%s %s' % (tag[0], tag[1]))
        
        # Thu Dec 14 20:53:56 2017 +0800
        post_date = post['published_date'].strftime("%b %d %H:%M:%S %Y +0800") 
        post_abbr_date = post['published_date'].strftime("%b %d, %Y")
        post_short_date = post['published_date'].strftime("%Y-%m-%d")
        title_escape = post['title'].replace('\\', '\\\\').replace('"', '\\"')
        t = t.replace('{{POST_TAGS}}', ' '.join(post_tags)).replace('{{VIDEO_URL}}', post['url']).replace('{{POST_TITLE_ESCAPE}}', title_escape).replace('{{POST_TITLE}}', post['title']).replace('{{POST_DATE}}', post_date).replace('{{POST_IMG}}', post['video_tn']).replace('{{VIDEO_EMBED_URL}}', post['video_embed_url']).replace('{{POST_ABBR_DATE}}', post_abbr_date).replace('{{VIDEO_DURATION}}', post['duration']).replace('{{POST_CATEGORY}}', post['category'])
        # print('%s' % t)
        filename = article_encode = post_short_date + '-' + 'video-' + str(vid) + '.markdown'
        print('filename: %s' % filename)
        fout = open(os.path.join(os.getenv('POSTS'), filename), 'w', encoding='utf-8')
        fout.write(t)
    
    print("time: ", time.time() - startTime)
    print('total people: %s' % sz)
