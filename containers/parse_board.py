#!/usr/bin/env python3

import json

with open ('lists/board.json') as f:
    lines = f.readlines()
    content = ''.join(lines)
    boards = json.loads(content)

    for cat in boards:
        print(cat)
        for board in boards[cat]:
            print(board)
